import { useEffect, useState } from 'react';
import { db } from './server/firebase';
import { collection, addDoc, deleteDoc, doc, getDocs, writeBatch } from 'firebase/firestore';
import { Route, Routes } from 'react-router-dom';
import Header from './components/Header';
import Drawer from './components/Drawer/Drawer';
import { Toaster } from 'react-hot-toast';
import TopHeader from './components/TopHeader';
import Home from './pages/Home';
import Favourites from './pages/Favourites';

function App() {
  const [cartOpened, setCartOpened] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [favourites, setFavourites] = useState([]);
  const [items, setItems] = useState([]);
  const [cartItems, setCartItems] = useState([]);
  const [searchValue, setSearchValue] = useState('');
  const [orderIdCounter, setOrderIdCounter] = useState(1);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [cartItemsData, favouritesData, itemsData] = await Promise.all([
          fetchCartItems(),
          fetchFavourites(),
          fetchItems(),
        ]);
        setCartItems(cartItemsData);
        setFavourites(favouritesData);
        setItems(itemsData);
      } catch (error) {
        console.error('Error fetching data: ', error);
      } finally {
        setIsLoading(false);
      }
    };

    const fetchCartItems = async () => {
      const cartItemsCollection = collection(db, 'cartItems');
      const snapshot = await getDocs(cartItemsCollection);
      return snapshot.docs.map((doc) => ({ docId: doc.id, ...doc.data() }));
    };

    const fetchItems = async () => {
      const itemsCollection = collection(db, 'products');
      const snapshot = await getDocs(itemsCollection);
      return snapshot.docs.map((doc) => doc.data());
    };

    const fetchFavourites = async () => {
      const favouritesCollection = collection(db, 'favourites');
      const snapshot = await getDocs(favouritesCollection);
      return snapshot.docs.map((doc) => ({ docId: doc.id, ...doc.data() }));
    };

    fetchData();
  }, []);

  const onAddtoCart = async (obj) => {
    try {
      const existingCartItem = cartItems.find((item) => item.id === obj.id);

      if (existingCartItem) {
        await deleteDoc(doc(db, 'cartItems', existingCartItem.docId));
        setCartItems((prev) => prev.filter((item) => item.id !== existingCartItem.id));
      } else {
        const docRef = await addDoc(collection(db, 'cartItems'), obj);
        const docId = docRef.id;
        setCartItems((prev) => [...prev, { ...obj, docId }]);
      }
    } catch (error) {
      console.error('Error adding document: ', error);
    }
  };

  const handleAddFavourite = async (obj) => {
    try {
      const existingFavourite = favourites.find((item) => item.id === obj.id);

      if (existingFavourite) {
        await deleteDoc(doc(db, 'favourites', existingFavourite.docId));
        setFavourites((prev) => prev.filter((item) => item.id !== existingFavourite.id));
      } else {
        const docRef = await addDoc(collection(db, 'favourites'), obj);
        const docId = docRef.id;
        setFavourites((prev) => [...prev, { ...obj, docId }]);
      }
    } catch (error) {
      console.error('Error adding document: ', error);
    }
  };

  const onRemoveItem = async (docId) => {
    try {
      console.log('Removing document with ID:', docId);
      await deleteDoc(doc(db, 'cartItems', docId));
      setCartItems((prev) => prev.filter((item) => item.docId !== docId));
      console.log('Document removed successfully.');
    } catch (error) {
      console.error('Error removing document: ', error);
    }
  };

  const onChangeSearchInput = (event) => {
    setSearchValue(event.target.value);
  };

  const onOrder = async () => {
    try {
      const ordersCollectionRef = collection(db, 'orders');

      await Promise.all(
        cartItems.map(async (item, index) => {
          const orderId = orderIdCounter + index;
          await addDoc(ordersCollectionRef, { ...item, orderId });
        }),
      );

      setOrderIdCounter((prevCounter) => prevCounter + cartItems.length);
      setCartItems([]);

      console.log('Order placed successfully.');
    } catch (error) {
      console.error('Error placing order:', error);
    }
  };

  return (
    <div className='wrapper clear'>
      <div>
        <Toaster
          position='bottom-right'
          reverseOrder={false}
          gutter={8}
          containerClassName=''
          containerStyle={{}}
          toastOptions={{
            className: '',
            duration: 5000,
            style: {
              background: '#000',
              color: '#fff',
              fontSize: '14px',
            },

            success: {
              duration: 2000,
              theme: {
                primary: 'green',
                secondary: 'black',
              },
            },
            error: {
              duration: 2000,
              theme: {
                primary: 'red',
                secondary: 'black',
              },
            },
          }}
        />
      </div>
      {cartOpened && (
        <Drawer
          items={cartItems}
          onClose={() => setCartOpened(false)}
          onRemove={onRemoveItem}
          onOrder={onOrder}
        />
      )}
      <TopHeader />
      <Header cartItemCount={cartItems.length} onClickCart={() => setCartOpened(true)} />
      <div className='p-30'>
        <Routes>
          <Route
            path='/favourites'
            element={<Favourites items={favourites} handleAddFavourite={handleAddFavourite} />}
          />
          <Route
            path='/'
            element={
              <Home
                isLoading={isLoading}
                cartItems={cartItems}
                items={items}
                searchValue={searchValue}
                setSearchValue={setSearchValue}
                onChangeSearchInput={onChangeSearchInput}
                handleAddFavourite={handleAddFavourite}
                onAddtoCart={onAddtoCart}
                onRemoveItem={onRemoveItem}
              />
            }
          />
        </Routes>
      </div>
    </div>
  );
}

export default App;
