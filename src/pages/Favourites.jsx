import React from 'react';
import Card from '../components/Card/Card';
import BackButton from '../assets/Group(1).svg';

import { Container, Heading, Text, Button } from '@chakra-ui/react';

const Favourites = ({ items, handleAddFavourite, favourites = [] }) => {
  return (
    <div className='d-flex align-start w100p flex-wrap'>
      {items.length > 0 ? (
        items.map((item) => (
          <Card
            key={item.id}
            id={item.id}
            title={item.title}
            price={item.price}
            imageUrl={item.imageUrl}
            docId={item.docId}
            favourited={true}
            onFavourite={handleAddFavourite}
          />
        ))
      ) : (
        <Container className='text-center align-center '>
          <Heading size='lg' fontSize='28px'>
            Закладок нет
          </Heading>
          <Text opacity='40%' fontSize='2xl'>
            Вы ничего не добавляли в закладки
          </Text>
          <Button
            marginTop={50}
            height='55px'
            width='425px'
            padding='25px'
            fontSize='14px'
            border='0px'
            fontWeight='600'
            bg='#000'
            color='#fff'
            _hover={{ bg: '#000' }}
          >
            Вернуться назад
          </Button>
        </Container>
      )}
    </div>
  );
};

export default Favourites;
