import React, { useEffect } from 'react';
import Card from '../components/Card/Card';
import Search from '../assets/Search.svg';
import ArrowDown from '../assets/chevron-down.svg';
import styles from '../components/Card/Card.module.scss';

import {
  Button,
  Heading,
  Input,
  Menu,
  CloseButton,
  MenuButton,
  MenuList,
  MenuItem,
} from '@chakra-ui/react';
import Carousel from '../components/Carousel/Carousel';
const Home = ({
  items,
  searchValue,
  setSearchValue,
  cartItems,
  onChangeSearchInput,
  handleAddFavourite,
  onAddtoCart,
  onRemoveItem,
  isLoading,
}) => {
  useEffect(() => {
    console.log('isLoading:', isLoading);
  }, [isLoading]);

  const renderItems = () => {
    const fakeItems = Array.from({ length: 12 }, (_, index) => ({
      id: index,
      title: 'Loading...',
      price: 0,
      imageUrl: 'loading-image-url',
    }));

    const filteredItems = items.filter((item) =>
      item.title.toLowerCase().includes(searchValue.toLowerCase()),
    );

    return isLoading
      ? fakeItems.map((item) => (
          <Card
            key={item.id}
            id={item.id}
            title={item.title}
            price={item.price}
            imageUrl={item.imageUrl}
            onFavourite={(obj) => handleAddFavourite(obj)}
            onPlus={(obj) => onAddtoCart(obj)}
            docId={item.docId}
            added={cartItems.some((obj) => Number(obj.id) === Number(item.id))}
            onRemove={onRemoveItem}
            loading={isLoading}
          />
        ))
      : filteredItems.map((item) => (
          <Card
            key={item.id}
            id={item.id}
            title={item.title}
            price={item.price}
            imageUrl={item.imageUrl}
            onFavourite={(obj) => handleAddFavourite(obj)}
            onPlus={(obj) => onAddtoCart(obj)}
            docId={item.docId}
            added={cartItems.some((obj) => Number(obj.id) === Number(item.id))}
            onRemove={onRemoveItem}
            loading={isLoading}
          />
        ));
  };

  return (
    <div className='content pb-40 pr-40 pl-40'>
      <div className='d-flex align-center  mb-40 justify-between'>
        <Heading size='lg' fontSize='28px'>
          {searchValue ? `Поиск по запросу: " ${searchValue}"` : 'Все кроссовки'}
        </Heading>
        <div className='d-flex align-center'>
          <Menu>
            <MenuButton
              as={Button}
              rightIcon={<img width={18} height={18} src={ArrowDown} />}
              className='text-uppercase menuButton'
              fontSize='11px'
              fontWeight='500'
              bg='#fff'
              _hover={{ bg: '#fff' }}
              _active={{ bg: '#fff' }}
              boxShadow='0px 0px 10px rgba(0, 0, 0, 0.07)'
            >
              Сортировать по: &nbsp;<span className='opacity-4'>Новизне</span>
            </MenuButton>
            <MenuList className='menuList'>
              <MenuItem className='menuItem' fontSize='12px'>
                Цене
              </MenuItem>
              <MenuItem className='menuItem' fontSize='12px'>
                Рейтингу
              </MenuItem>
            </MenuList>
          </Menu>
          <div className='search-block d-flex ml-10'>
            <img height={22} width={22} src={Search} alt='Search' />
            <Input
              placeholder='Поиск...'
              onChange={onChangeSearchInput}
              value={searchValue}
              _placeholder={{ opacity: 1, color: '#C4C4C4' }}
            />
            {searchValue && (
              <CloseButton onClick={() => setSearchValue('')} className='closeSearch' size='sm' />
            )}
          </div>
        </div>
      </div>
      {/* Card  */}
      <div className={`d-flex align-start justify-center  w100 flex-wrap ${styles.cardContainer}`}>
        {renderItems()}
      </div>
    </div>
  );
};

export default Home;
