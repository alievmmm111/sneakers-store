import React from 'react';
import Add from '../../assets/Icon(1).svg';
import AddChecked from '../../assets/Icon(3).svg';
import Like from '../../assets/Like.svg';
import LikeActive from '../../assets/LikeActive.svg';
import { Text, Button } from '@chakra-ui/react';
import toast from 'react-hot-toast';
import styles from './Card.module.scss';

import ContentLoader from 'react-content-loader';

const Card = ({
  id,
  title,
  imageUrl,
  price,
  onPlus,
  onFavourite,
  onRemove,
  docId,
  favourited = false,
  added = false,
  loading = false,
}) => {
  const [isAdded, setIsAdded] = React.useState(added);
  const [isFavourite, setIsFavourite] = React.useState(favourited);
  const formattedPrice = price ? price.toLocaleString() : '';

  React.useEffect(() => {
    setIsAdded(added);
  }, [added]);

  const onClickFavourite = () => {
    onFavourite({ id, title, price, imageUrl });
    setIsFavourite((prevIsFavourite) => !prevIsFavourite);
  };
  const onClickPlus = async () => {
    if (isAdded) return;

    setIsAdded(true);

    try {
      await onPlus({ id, title, imageUrl, price });
      toast.success(
        <p>
          {title} <br />
          Успешно добавлено!
        </p>,
      );
    } catch (error) {
      console.error('Error adding item to cart: ', error);
      toast.error('Произошла ошибка при добавлении товара в корзину');
    } finally {
      setIsAdded(false);
    }
  };

  return (
    <div>
      <div className={styles.card}>
        {loading ? (
          <ContentLoader
            speed={1.5}
            width={250}
            height={241}
            viewBox='0 0 240 250'
            backgroundColor='#f3f3f3'
            foregroundColor='#ecebeb'
          >
            <rect x='307' y='40' rx='40' ry='40' width='250' height='278' />
            <rect x='0' y='10' rx='10' ry='10' width='198' height='125' />
            <rect x='0' y='155' rx='3' ry='3' width='198' height='15' />
            <rect x='0' y='175' rx='3' ry='3' width='111' height='15' />
            <rect x='0' y='215' rx='8' ry='8' width='95' height='24' />
            <rect x='160' y='210' rx='8' ry='8' width='40' height='40' />
          </ContentLoader>
        ) : (
          <>
            <Button
              className={styles.cardLike}
              onClick={onClickFavourite}
              border='1px solid #f2f2f2'
              padding='0px'
              borderRadius='8px'
              bg='#fff'
              _hover={{ bg: '#fff' }}
              fontWeight='500'
            >
              <img src={isFavourite ? LikeActive : Like} width={16} height={16} />
            </Button>
            <img width={200} height={200} src={imageUrl} alt='' />
            <Text fontSize='14px' className={styles.cardTitle}>
              {title}
            </Text>
            <br />
            <div className='d-flex justify-between align-center'>
              <div className='d-flex flex-column'>
                <b className={styles.cardPrice}>от {formattedPrice} ₽ </b>
              </div>
              <Button
                className='ml-25'
                onClick={onClickPlus}
                isLoading={loading}
                fontSize='14px'
                border={isAdded ? 'none' : '1px solid #f2f2f2'}
                borderRadius='8px'
                padding='0px'
                bg={isAdded ? '#000' : '#fff'}
                _hover={{ bg: isAdded ? '#000' : '#fff' }}
                fontWeight='500'
              >
                <img
                  src={isAdded ? AddChecked : Add}
                  width={isAdded ? 18 : 13}
                  height={isAdded ? 18 : 13}
                />
              </Button>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default Card;
