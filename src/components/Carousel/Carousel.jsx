import React from 'react';
import styles from './Carousel.module.scss';

const Carousel = () => {
  return (
    <div className={styles.carousel}>
      <img src='/public/sneakers/banner.png' alt='' />
    </div>
  );
};

export default Carousel;
