import { Link } from 'react-router-dom'
import Logo from '../assets/logo1.png'
import Cart from '../assets/Cart.svg'
import Like from '../assets/LikeHeader.svg'
import Profile from '../assets/Profile.svg'
import Drop from '../assets/wallet-01.svg'
import { Text, Button, Badge,

} from '@chakra-ui/react'

const Header = (props) => {
    return (
        <header className='d-flex justify-between align-center p-15'>
        <div className="d-flex align-center">
          <Link className="d-flex align-center" to='/'>
        <img width={40} height={40} src={Logo} alt="" />
       <div>
        <Text fontSize='lg' className='logo text-uppercase'>Sneakers</Text>
        <Text fontSize='xs' className='logoSlogan'>Магазин хайп кроссовок</Text>
       </div>
       </Link>
       <Button className='ml-25'
        height='40px'
        width='130px'
        fontSize='14px'
        border='0px'
        fontWeight='500'
        bg='#000'
        color='#fff' _hover={{bg: '#000'}}
        >
        <img height={18} width={18} src={Drop} alt="" />
        Каталог
       </Button>
        </div>
       <ul className="d-flex">
        <li className='mr-10 d-flex align-center' onClick={props.onClickCart}>
          <div className='cart' >
          <img src={Cart} height={24} width={24} />
          <Badge className='cartBadge' >{props.cartItemCount}</Badge>
          </div>
          <Text fontSize='13px' className='cardTitle'>Корзина</Text>
        </li>
        <li >
          <Link to='/favourites' className='mr-10 d-flex align-center'>
          <img src={Like} height={24} width={24} />
          <Text fontSize='13px' className='cardTitle'>Закладки</Text>
          </Link>
        </li>
        <li className='mr-10 d-flex align-center'>
        <img src={Profile} height={24} width={24} alt="" />
        <Text fontSize='13px' className='cardTitle'>Профиль</Text>
        </li>
       </ul>
      </header>
    );
}

export default Header;
