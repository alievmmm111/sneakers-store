import Box from '../../assets/package.png';
import OrderDone from '../../assets/fast-delivery.png';
import BackButton from '../../assets/Group(1).svg';
import { Text, Button, Heading, CloseButton } from '@chakra-ui/react';
import styles from './Drawer.module.scss';
import { useState } from 'react';
import { db } from '../../server/firebase';
import { collection, addDoc, deleteDoc, doc, getDocs, writeBatch } from 'firebase/firestore';

const Drawer = ({ onClose, onRemove, items = [], onOrder }) => {
  const calculateTotal = () => {
    let total = 0;
    items.forEach((item) => {
      total += item.price; // Предполагается, что у каждого объекта в массиве items есть свойство price
    });
    return total;
  };

  const [isOrderComplete, setIsOrderComplete] = useState(false);

  const onClickOrder = () => {
    console.log('Order sent');
    onOrder();
    setIsOrderComplete(true);
  };

  return (
    <div className={styles.overlay}>
      <div className={styles.drawer}>
        <Heading className='mb-30 d-flex justify-between' size='lg' fontSize='24px'>
          Корзина
          <CloseButton size='md' onClick={onClose} />
        </Heading>
        {items.length > 0 ? (
          <div>
            <div className={styles.items}>
              {items.map((obj) => (
                <div className={styles.cartItem} key={obj.id}>
                  <div
                    style={{ backgroundImage: `url(${obj.imageUrl})` }}
                    className={styles.cartItemImg}
                  ></div>
                  <div className='mr-20 flex'>
                    <Text fontSize='13px' className='cardTitle mb-5'>
                      {obj.title}
                    </Text>
                    <b className='cardPrice cartItemPrice'>{obj.price} ₽ </b>
                  </div>
                  <CloseButton
                    border='1px solid #F2F2F2'
                    size='md'
                    onClick={() => onRemove(obj.docId)}
                  />
                </div>
              ))}
            </div>
            <div className={styles.cartTotalBlock}>
              <ul>
                <li>
                  <span>Итого:</span>
                  <div></div>
                  <b>{calculateTotal()} ₽</b>
                </li>
                <li className='d-flex'>
                  <span>Налог 5%:</span>
                  <div></div>
                  <b>{(calculateTotal() * 0.05).toFixed(2)} ₽</b> {/* Рассчитываем налог 5% */}
                </li>
              </ul>
              <Button
                onClick={onClickOrder}
                height='40px'
                width='100%'
                fontSize='14px'
                border='0px'
                fontWeight='600'
                bg='#000'
                color='#fff'
                _hover={{ bg: '#000' }}
              >
                Оформить заказ
              </Button>
            </div>
          </div>
        ) : (
          <div
            className={`d-flex align-center justify-center flex-column flex ${styles.cartEmpty}`}
          >
            <img
              src={isOrderComplete ? OrderDone : Box}
              width={200}
              height={200}
              className='mb-20'
            />
            <Heading size='lg' fontSize='24px'>
              {isOrderComplete ? 'Заказ Оформлен' : 'Корзина пуста'}
            </Heading>
            <span>
              {isOrderComplete
                ? 'Ваш заказ #18 скоро будет передан курьерской доставке'
                : 'Добавьте хотя бы одну пару кроссовок, чтобы сделать заказ.'}
            </span>
            <Button
              onClick={onClose}
              height='40px'
              width='100%'
              padding='25px'
              fontSize='14px'
              border='0px'
              fontWeight='600'
              bg='#000'
              color='#fff'
              _hover={{ bg: '#000' }}
            >
              <img src={BackButton} />
              Вернуться назад
            </Button>
          </div>
        )}
      </div>
    </div>
  );
};

export default Drawer;
