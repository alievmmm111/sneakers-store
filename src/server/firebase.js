// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyBcTHzqp4P7O_k5laSu6Kmb-FxhLA4SC5I',
  authDomain: 'sneakers-f95ba.firebaseapp.com',
  projectId: 'sneakers-f95ba',
  storageBucket: 'sneakers-f95ba.appspot.com',
  messagingSenderId: '492992976899',
  appId: '1:492992976899:web:7f87f19b98c48f36411636',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
